"use strict"

// Services

const buttons = document.querySelectorAll(".service_item");
buttons.forEach((btn) => {

	btn.addEventListener('click', clickButton);

})

function clickButton(event) {
	event.preventDefault();
	let buttonatrr = this.getAttribute('data-name');
	let text = document.querySelector(`.service-img-list [data-name = ${buttonatrr}]`);
	document.querySelector('.seen').classList.remove('seen');
	text.classList.add('seen');
	document.querySelector('.service_item.active').classList.remove('active');
	this.classList.add('active');
}

// <<<<<<<<<<Work>>>>>>>>>>


const workButtons = document.querySelectorAll(".section-work-item");
const workImages = document.querySelectorAll('.work-images-item');
workButtons.forEach((btn) => {
	btn.addEventListener('click', clickWorkButton);
})

function clickWorkButton(event) {
	event.preventDefault();
	let workImagesAtrr = this.getAttribute('data-work');

	workImages.forEach((item) => {
			let workItem = item.getAttribute("data-work");
			let active = document.querySelector(".section-work-item-active");
			active.classList.remove('section-work-item-active');
			this.classList.add('section-work-item-active');

			workImagesAtrr === workItem ? item.classList.remove("hide") : item.classList.add("hide");
			if (workImagesAtrr === "All") {
				item.classList.remove("hide");
			}
		}
	)
}

// Load images
const images = [
	"./images/loadimage/wordpress4.jpg",
	"./images/loadimage/wordpress5.jpg",
	"./images/loadimage/wordpress6.jpg",
	"./images/loadimage/wordpress7.jpg",
	"./images/loadimage/wordpress8.jpg",
	"./images/loadimage/wordpress9.jpg",
	"./images/loadimage/wordpress10.jpg",
	"./images/loadimage/wordpress11.jpg",
	"./images/loadimage/wordpress12.jpg",
	"./images/loadimage/wordpress13.jpg",
	"./images/loadimage/wordpress14.jpg",
	"./images/loadimage/wordpress15.jpg",
]
const btn = document.querySelector(".work-button")

const loadbtn = (array, attribute) => {
	array.forEach((item) => {

		let ul = document.querySelector('.work-images-list');
		let li = ul.querySelector('.work-images-item');
		let liNode = li.cloneNode(true);
		let imageNode = liNode.querySelector('img');
		imageNode.src = item;
		liNode.setAttribute('data-work', attribute);
		ul.append(liNode);
	})

}
btn.addEventListener('click', (event) => {
	event.preventDefault();
	loadbtn(images, 'Wordpress')
	btn.remove();
})

// People >>>>>>>>

const tapeButtons = document.querySelectorAll(".people-tape-item");
tapeButtons.forEach((btn) => {

	btn.addEventListener('click', clickTape);

})

function clickTape(event) {
	event.preventDefault();
	let tapeImageAtrr = this.getAttribute('data-people');
	let bigImages = document.querySelectorAll('.people-item');
	bigImages.forEach((item) => {
		let bigImageAtrr = item.getAttribute('data-people');
		bigImageAtrr === tapeImageAtrr ? item.classList.add('people-active') : item.classList.remove('people-active');
	})
	document.querySelector('.people-tape-item.active-img-tape').classList.remove('active-img-tape');
	this.classList.add('active-img-tape');
}

// Tape Buttons
const slides = document.querySelectorAll(".people-item");
const tapeSlides = document.querySelectorAll(".people-tape li");
let sliderCount = 0;

function slider(item) {
	if (item === "right") {
		sliderCount++;
		if (sliderCount === tapeSlides.length) {
			sliderCount = 0;
		}
	} else {
		sliderCount === 0 ? sliderCount = tapeSlides.length - 1 : sliderCount--;
	}

	for (let i = 0; i < tapeSlides.length; i++) {
		tapeSlides[i].classList.remove("active-img-tape");
		slides[i].classList.remove("people-active");
	}
	tapeSlides[sliderCount].classList.add("active-img-tape");
	slides[sliderCount].classList.add("people-active");
}

const btnRight = document.querySelector(".btn-right");
const btnLeft = document.querySelector(".btn-left");

btnRight.addEventListener("click", () => {
	slider("right")
});
btnLeft.addEventListener("click", () => {
	slider("left");
});

